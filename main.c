#include "get_next_line_bonus.h"
#include <stdio.h>

void	ft_putendl(char *str)
{
	write(1, str, ft_strlen(str));
	write(1, "\n", 1);
}

int		main(int argc, char **argv)
{
	int		fd;
	char	*line;
	// line = "salut";
	line = 0;
	int ret = 1;
	fd = open(argv[argc - 1], O_RDONLY);
	ret = 1;
	while (ret > 0)
	{
		ret = get_next_line(fd, &line);
		((ret == 1) ? write(1, "[1] ", 5) : write(1, "[0] ", 5));
		ft_putendl(line);
		free(line);
	}
	if (close(fd) == -1)
		return (1);
	// system("leaks a.out");
	return (0);
}
